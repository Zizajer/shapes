﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialManager : MonoBehaviour
{
    private int tutorialStepCounter;
    public Vector2Int[] PositionsForPlayer;
    public GameObject Marker;
    private List<GameObject> createdMarkers;
    private Vector2Int chosenPosition;
    private bool isTutorialEnded;

    // Start is called before the first frame update
    void Start()
    {
        createdMarkers = new List<GameObject>();
        tutorialStepCounter = 0;
        isTutorialEnded = false;

        if (PlayerPrefs.GetInt("IsTutorialCompleted") == 0)
            PlayerPrefs.SetInt("IsTutorialCompleted", 1);
    }

    public void GoToNextPoint(GameObject[,] tiles, Vector2Int lastPosition)
    {
        chosenPosition = lastPosition;
        if (isTutorialEnded)
        {
            Invoke("GoToMainMenu", 1.2f);
        }
        else if (tutorialStepCounter == 8)
        {
            foreach (GameObject tile in tiles)
            {
                tile.GetComponent<Tile>().isSelectable = false;
            }

            foreach (GameObject mark in createdMarkers)
            {
                Destroy(mark);
            }

            createdMarkers = new List<GameObject>();
            tiles[PositionsForPlayer[tutorialStepCounter].x, PositionsForPlayer[tutorialStepCounter].y].GetComponent<Tile>().isSelectable = true;
            createdMarkers.Add(Instantiate(Marker, tiles[PositionsForPlayer[tutorialStepCounter].x, PositionsForPlayer[tutorialStepCounter].y].transform.position, new Quaternion(0, 0, 0, 0)));
            tiles[PositionsForPlayer[tutorialStepCounter+1].x, PositionsForPlayer[tutorialStepCounter+1].y].GetComponent<Tile>().isSelectable = true;
            createdMarkers.Add(Instantiate(Marker, tiles[PositionsForPlayer[tutorialStepCounter+1].x, PositionsForPlayer[tutorialStepCounter+1].y].transform.position, new Quaternion(0, 0, 0, 0)));
            tiles[PositionsForPlayer[tutorialStepCounter+2].x, PositionsForPlayer[tutorialStepCounter+2].y].GetComponent<Tile>().isSelectable = true;
            createdMarkers.Add(Instantiate(Marker, tiles[PositionsForPlayer[tutorialStepCounter+2].x, PositionsForPlayer[tutorialStepCounter+2].y].transform.position, new Quaternion(0, 0, 0, 0)));
            tiles[PositionsForPlayer[tutorialStepCounter+3].x, PositionsForPlayer[tutorialStepCounter+3].y].GetComponent<Tile>().isSelectable = true;
            createdMarkers.Add(Instantiate(Marker, tiles[PositionsForPlayer[tutorialStepCounter+3].x, PositionsForPlayer[tutorialStepCounter+3].y].transform.position, new Quaternion(0, 0, 0, 0)));
            tiles[PositionsForPlayer[tutorialStepCounter+4].x, PositionsForPlayer[tutorialStepCounter+4].y].GetComponent<Tile>().isSelectable = true;
            createdMarkers.Add(Instantiate(Marker, tiles[PositionsForPlayer[tutorialStepCounter+4].x, PositionsForPlayer[tutorialStepCounter+4].y].transform.position, new Quaternion(0, 0, 0, 0)));
            tutorialStepCounter+=5;
        }
        else if(tutorialStepCounter == 8+5)
        {
            foreach (GameObject tile in tiles)
            {
                tile.GetComponent<Tile>().isSelectable = false;
            }

            foreach (GameObject mark in createdMarkers)
            {
                Destroy(mark);
            }

            createdMarkers = new List<GameObject>();

            if (chosenPosition == PositionsForPlayer[10] || chosenPosition == PositionsForPlayer[11] || chosenPosition == PositionsForPlayer[12])
            {
                tiles[PositionsForPlayer[tutorialStepCounter].x, PositionsForPlayer[tutorialStepCounter].y].GetComponent<Tile>().isSelectable = true;
                createdMarkers.Add(Instantiate(Marker, tiles[PositionsForPlayer[tutorialStepCounter].x, PositionsForPlayer[tutorialStepCounter].y].transform.position, new Quaternion(0, 0, 0, 0)));
            }
            else if (chosenPosition == PositionsForPlayer[8] || chosenPosition == PositionsForPlayer[9])
            {
                tiles[PositionsForPlayer[tutorialStepCounter + 1].x, PositionsForPlayer[tutorialStepCounter + 1].y].GetComponent<Tile>().isSelectable = true;
                createdMarkers.Add(Instantiate(Marker, tiles[PositionsForPlayer[tutorialStepCounter + 1].x, PositionsForPlayer[tutorialStepCounter + 1].y].transform.position, new Quaternion(0, 0, 0, 0)));
            }

            tutorialStepCounter += 2;
        }
        else if (tutorialStepCounter == 8 + 5 + 2)
        {
            foreach (GameObject tile in tiles)
            {
                tile.GetComponent<Tile>().isSelectable = false;
            }

            foreach (GameObject mark in createdMarkers)
            {
                Destroy(mark);
            }

            createdMarkers = new List<GameObject>();
            if (chosenPosition == PositionsForPlayer[13])
            {
                tiles[PositionsForPlayer[8].x, PositionsForPlayer[8].y].GetComponent<Tile>().isSelectable = true;
                createdMarkers.Add(Instantiate(Marker, tiles[PositionsForPlayer[8].x, PositionsForPlayer[8].y].transform.position, new Quaternion(0, 0, 0, 0)));
                tiles[PositionsForPlayer[9].x, PositionsForPlayer[9].y].GetComponent<Tile>().isSelectable = true;
                createdMarkers.Add(Instantiate(Marker, tiles[PositionsForPlayer[9].x, PositionsForPlayer[9].y].transform.position, new Quaternion(0, 0, 0, 0)));
            }
            if (chosenPosition == PositionsForPlayer[14])
            {
                tiles[PositionsForPlayer[10].x, PositionsForPlayer[10].y].GetComponent<Tile>().isSelectable = true;
                createdMarkers.Add(Instantiate(Marker, tiles[PositionsForPlayer[10].x, PositionsForPlayer[10].y].transform.position, new Quaternion(0, 0, 0, 0)));
                tiles[PositionsForPlayer[11].x, PositionsForPlayer[11].y].GetComponent<Tile>().isSelectable = true;
                createdMarkers.Add(Instantiate(Marker, tiles[PositionsForPlayer[11].x, PositionsForPlayer[11].y].transform.position, new Quaternion(0, 0, 0, 0)));
                tiles[PositionsForPlayer[12].x, PositionsForPlayer[12].y].GetComponent<Tile>().isSelectable = true;
                createdMarkers.Add(Instantiate(Marker, tiles[PositionsForPlayer[12].x, PositionsForPlayer[12].y].transform.position, new Quaternion(0, 0, 0, 0)));
            }
            tutorialStepCounter += 5;
        }
        else if (tutorialStepCounter == 8 + 5 + 2 + 5)
        {
            foreach (GameObject tile in tiles)
            {
                tile.GetComponent<Tile>().isSelectable = false;
            }

            foreach (GameObject mark in createdMarkers)
            {
                Destroy(mark);
            }

            createdMarkers = new List<GameObject>();

            if (chosenPosition == PositionsForPlayer[10] || chosenPosition == PositionsForPlayer[11] || chosenPosition == PositionsForPlayer[12])
            {
                tiles[PositionsForPlayer[13].x, PositionsForPlayer[13].y].GetComponent<Tile>().isSelectable = true;
                createdMarkers.Add(Instantiate(Marker, tiles[PositionsForPlayer[13].x, PositionsForPlayer[13].y].transform.position, new Quaternion(0, 0, 0, 0)));
            }
            else if (chosenPosition == PositionsForPlayer[8] || chosenPosition == PositionsForPlayer[9])
            {
                tiles[PositionsForPlayer[14].x, PositionsForPlayer[14].y].GetComponent<Tile>().isSelectable = true;
                createdMarkers.Add(Instantiate(Marker, tiles[PositionsForPlayer[14].x, PositionsForPlayer[14].y].transform.position, new Quaternion(0, 0, 0, 0)));
            }

            tutorialStepCounter += 2;
            isTutorialEnded = true;

        }
        else
        {
            foreach (GameObject tile in tiles)
            {
                tile.GetComponent<Tile>().isSelectable = false;
            }

            foreach (GameObject mark in createdMarkers)
            {
                Destroy(mark);
            }

            createdMarkers = new List<GameObject>();

            tiles[PositionsForPlayer[tutorialStepCounter].x, PositionsForPlayer[tutorialStepCounter].y].GetComponent<Tile>().isSelectable = true;
            createdMarkers.Add(Instantiate(Marker, tiles[PositionsForPlayer[tutorialStepCounter].x, PositionsForPlayer[tutorialStepCounter].y].transform.position, new Quaternion(0, 0, 0, 0)));
            tutorialStepCounter++;
        }
        
    }

    private void GoToMainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
