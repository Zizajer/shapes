﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shape : MonoBehaviour
{
    public SpriteRenderer SpriteRenderer;
    private bool isMoving = false;
    private List<Tile> waypoints;
    private int waypointIndex;
    public float MovementSpeed;
    private GameManager gameManager;

    private void Start()
    {
        gameManager = GetComponentInParent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isMoving)
            if (waypointIndex <= waypoints.Count - 1)
            {
                transform.position = Vector3.MoveTowards(transform.position,
                    waypoints[waypointIndex].transform.position,
                    MovementSpeed * Time.deltaTime);

                if (transform.position == waypoints[waypointIndex].transform.position)
                {
                    waypointIndex += 1;
                }
            }
            else
            {
                GetComponentInParent<Tile>().IsShapeAcquired = true;
                if (gameManager.CheckForLines(transform.GetComponentInParent<Tile>()))
                {   
                    gameManager.gameState = GameManager.GameStates.NoneSelected;
                }
                else
                {
                    gameManager.AddShapesAndCheckForLines();
                }

                if (gameManager.TutorialManager != null)
                {
                    StartCoroutine(gameManager.GoToNextTutorialPoint(new Vector2Int(GetComponentInParent<Tile>().PositionX, GetComponentInParent<Tile>().PositionY), 0.1f));
                }

                isMoving = false;
            }
    }

    public void StartMoving(List<Tile> waypointsToMove)
    {
        waypoints = waypointsToMove;
        waypointIndex = 0;
        isMoving = true;
    }

    public Color GetColor()
    {
        return SpriteRenderer.color;
    }

    public Sprite GetSprite()
    {
        return SpriteRenderer.sprite;
    }

    public void PlayCollapseAnimation()
    {
        GetComponent<Animator>().SetBool("isReadyToCollapse",true);
    }

    public void DestroyShape()
    {
        Destroy(transform.gameObject);
    }
}
