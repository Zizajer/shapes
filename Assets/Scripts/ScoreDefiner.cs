﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreDefiner 
{
    public static int GetScoreForLine(List<Tile> tilesInLine)
    {
        int score = 0;
        int currentScoreForShape = 10;
        bool isMatchInColor = true;
        bool isMatchInShape = true;
        Color colorAtFirstTile = tilesInLine[0].GetShapeFromTile().GetColor();
        Sprite shapeAtFirstTile = tilesInLine[0].GetShapeFromTile().GetSprite();
		
        foreach(Tile tile in tilesInLine)
        {
            if (colorAtFirstTile == tile.GetShapeFromTile().GetColor() && isMatchInColor)
                isMatchInColor = true;
            else
                isMatchInColor = false;

            if (shapeAtFirstTile == tile.GetShapeFromTile().GetSprite() && isMatchInShape)
                isMatchInShape = true;
            else
                isMatchInShape = false;

            score += currentScoreForShape;
            currentScoreForShape += 10;
        }

        if (isMatchInColor)
        {
            score += 500;
            AchievementManager.UnlockLineInSameColorkAchievement();
        }

        if (isMatchInShape)
        {
            score += 500;
            AchievementManager.UnlockLineInSameShapekAchievement();
        }

        if (isMatchInShape && isMatchInColor)
        {
            score += 3000;
            AchievementManager.UnlockLineInSameColorAndShapekAchievement();
        }

        return score;
    }
}
