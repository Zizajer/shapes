﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeButton : MonoBehaviour
{
    public Image image;
    public Sprite whenOnButton;
    public Sprite whenOffButton;
    
    public void DefineSpriteForButton(bool condition)
    {
        if (condition)
            image.sprite = whenOnButton;
        else
            image.sprite = whenOffButton;
    }
}
