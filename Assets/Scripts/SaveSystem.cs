﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class SaveSystem
{
    public static void SaveCurrentGameToFile(SavedGameModel savedGameModel)
    {
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        string path = Path.Combine(Application.persistentDataPath, "savedGame.dat");

        FileStream stream = new FileStream(path, FileMode.Create);

        binaryFormatter.Serialize(stream, savedGameModel);

        stream.Close();
    }

    public static SavedGameModel LoadPreviousGameFromFile()
    {
        string path = Path.Combine(Application.persistentDataPath, "savedGame.dat");

        if (File.Exists(path))
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            SavedGameModel savedGameModel = binaryFormatter.Deserialize(stream) as SavedGameModel;
            stream.Close();
            return savedGameModel;
        }
        else
        {
            return null;
        }
    }

    public static void DeleteSaveGameFile()
    {
        string path = Path.Combine(Application.persistentDataPath, "savedGame.dat");

        try
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }
        catch (IOException)
        {
            Debug.Log("File not found at : " + path);
        }
    }
}
