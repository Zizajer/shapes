﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject EndGamePanel;
    public GameObject PauseGamePanel;
    public Text scoreText;
    public Text highScoreText;
    public ChangeButton musicButton;
    public ChangeButton SoundEffectsButton;
    public AudioManager audioManager;
    public GameManager tileGrid;
    public Score score;
    public bool isUIActive;
    private bool isMusicOn;
    public AdManager AdManager;

    void Start()
    {
        highScoreText.text = PlayerPrefs.GetInt("HighScore").ToString();
        isUIActive = false;

        isMusicOn = (PlayerPrefs.GetInt("isMusicOn") == 1) ? true : false;

        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();

        if (SceneManager.GetActiveScene().name.Equals("MenuScene"))
        {
            Social.localUser.Authenticate(success =>
            {
                if (PlayerPrefs.GetInt("IsTutorialCompleted") == 0)
                    StartTutorialScene();

            });
        }

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (SceneManager.GetActiveScene().name.Equals("MenuScene"))
                QuitGame();
            else if (SceneManager.GetActiveScene().name.Equals("TutorialScene"))
                SceneManager.LoadScene(0);
            else
            {
                if (!PauseGamePanel.activeInHierarchy)
                    GoToMainMenu();
                else if (PauseGamePanel.activeInHierarchy)
                    DeactivePauseGamePanel();
            }
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ActiveEndGamePanel(int score)
    {
        EndGamePanel.SetActive(true);
        musicButton.DefineSpriteForButton(isMusicOn);
        SoundEffectsButton.DefineSpriteForButton(audioManager.isSoundEffectsOn);
        scoreText.text = score.ToString();
        if(PlayerPrefs.GetInt("HighScore") < score)
        {
            PlayerPrefs.SetInt("HighScore", score);
        }
        highScoreText.text = PlayerPrefs.GetInt("HighScore").ToString();
        isUIActive = true;

        Social.ReportScore(score, GPGSIds.leaderboard_score_leaderboard, (bool success) => {
            // handle success or failure
        });

        SaveSystem.DeleteSaveGameFile();

        if (AdManager != null)
            AdManager.ShowInterstitialAd();
    }

    public void ActivePauseGamePanel()
    {
        PauseGamePanel.SetActive(true);
        SoundEffectsButton.DefineSpriteForButton(audioManager.isSoundEffectsOn);
        musicButton.DefineSpriteForButton(isMusicOn);
        isUIActive = true;
    }

    public void DeactivePauseGamePanel()
    {
        PauseGamePanel.GetComponent<Animator>().SetBool("isReadyToHide",true);
        isUIActive = false;
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void RestartGame()
    {
        if (PlayerPrefs.GetInt("HighScore") < score.GetScore())
        {
            PlayerPrefs.SetInt("HighScore", score.GetScore());
        }
        SaveSystem.DeleteSaveGameFile();
        SceneManager.LoadScene(1);
    }

    public void GoToMainMenu()
    {
        SaveSystem.SaveCurrentGameToFile(tileGrid.SaveCurrentGame());
        SceneManager.LoadScene(0);
    }

    public void GoToMainMenuWithoutSaving()
    {
        SceneManager.LoadScene(0);
    }

    public void GoToMainMenuAndDeleteSaveFile()
    {
        SaveSystem.DeleteSaveGameFile();
        SceneManager.LoadScene(0);
    }

    public void StartTutorialScene()
    {
        SceneManager.LoadScene(2);
    }

    public void ShowScores()
    {
        Social.ShowLeaderboardUI();
    }

    public void ShowAchievements()
    {
        Social.ShowAchievementsUI();
    }

    public void ChangeIsMusicOnStatus()
    {
        isMusicOn = !isMusicOn;

        if (isMusicOn)
            PlayerPrefs.SetInt("isMusicOn", 1);
        else
            PlayerPrefs.SetInt("isMusicOn", 0);

        musicButton.DefineSpriteForButton(isMusicOn);
    }

    public void ChangeIsSoundEffectsOnStatus()
    {
        audioManager.isSoundEffectsOn = !audioManager.isSoundEffectsOn;

        if (audioManager.isSoundEffectsOn)
            PlayerPrefs.SetInt("isSoundEffectsOn", 1);
        else
            PlayerPrefs.SetInt("isSoundEffectsOn", 0);

        SoundEffectsButton.DefineSpriteForButton(audioManager.isSoundEffectsOn);
    }
}
