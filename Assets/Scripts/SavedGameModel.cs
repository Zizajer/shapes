﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SavedGameModel
{
    public List<ShapeData> ShapeDatas;
    public int CurrentScore;

    public SavedGameModel(List<ShapeData> shapeDatas, int currentScore)
    {
        ShapeDatas = shapeDatas;
        CurrentScore = currentScore;
    }
}
