﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{

    public bool IsSelected;
    [SerializeField]
    private bool isShapeAcquired = false;
    public SpriteRenderer SpriteRenderer;
    public Color UnselectedTileColor;
    public Color SelectedTileColor;
    public int PositionX;
    public int PositionY;
    public float DistanceToTarget;
    public Tile Parent;
    public bool isSelectable;
    private GameManager gameManager;
    private PathGenerator pathGenerator;
    public bool IsShapeAcquired { get => isShapeAcquired; set => isShapeAcquired = value; }

    void Start()
    { 
        IsSelected = false;
        isSelectable = true;
        gameManager = GetComponentInParent<GameManager>();
        pathGenerator = new PathGenerator(gameManager);
    }


    private void OnMouseDown()
    {
        if (!gameManager.uIManager.isUIActive && isSelectable) { 
            if (IsShapeAcquired && GetComponentInParent<GameManager>().gameState.Equals(GameManager.GameStates.NoneSelected))
            {
                gameManager.gameState = GameManager.GameStates.FirstSelected;
                gameManager.SelectedShape = transform.GetChild(0).gameObject;
                gameManager.FirstSelectedTile = this.gameObject;
                ReverseSelected();

                GetComponentInParent<AudioManager>().PlayShapeSelectedClip();

                if (gameManager.TutorialManager != null)
                {
                    StartCoroutine(gameManager.GoToNextTutorialPoint(new Vector2Int(PositionX, PositionY), 0.1f));
                }
            }
            else if (IsShapeAcquired && IsSelected && gameManager.gameState.Equals(GameManager.GameStates.FirstSelected))
            {
                gameManager.gameState = GameManager.GameStates.NoneSelected;
                ReverseSelected();

                if (gameManager.TutorialManager != null)
                {
                    StartCoroutine(gameManager.GoToNextTutorialPoint(new Vector2Int(PositionX, PositionY), 0.1f));
                }
            }
            else if (IsShapeAcquired && GetComponentInParent<GameManager>().gameState.Equals(GameManager.GameStates.FirstSelected))
            {
                gameManager.SelectedShape = transform.GetChild(0).gameObject;
                gameManager.FirstSelectedTile.GetComponent<Tile>().ReverseSelected();
                gameManager.FirstSelectedTile = this.gameObject;
                ReverseSelected();
                
                GetComponentInParent<AudioManager>().PlayShapeSelectedClip();

                if (gameManager.TutorialManager != null)
                {
                    StartCoroutine(gameManager.GoToNextTutorialPoint(new Vector2Int(PositionX, PositionY), 0.1f));
                }
            }

            if (!IsShapeAcquired && gameManager.gameState == GameManager.GameStates.FirstSelected)
            {
                gameManager.gameState = GameManager.GameStates.SecondSelected;
                gameManager.SecondSelectedTile = this.gameObject;
                List<Tile> path = pathGenerator.GeneratePath();
                if (path != null)
                {
                    gameManager.SelectedShape.transform.parent.GetComponent<Tile>().ReverseSelected();
                    gameManager.SelectedShape.transform.parent.GetComponent<Tile>().IsShapeAcquired = false;
                    gameManager.SelectedShape.transform.SetParent(transform);
                    transform.GetChild(0).GetComponent<Shape>().StartMoving(path);
                    gameManager.gameState = GameManager.GameStates.ShapeMoving;
                    GetComponentInParent<AudioManager>().PlayFreeTileSelectedClip();
                }
                else
                {
                    gameManager.gameState = GameManager.GameStates.FirstSelected;
                    GetComponentInParent<AudioManager>().PlayCantMoveThereClip();

                    if (gameManager.TutorialManager != null)
                    {
                        StartCoroutine(gameManager.GoToNextTutorialPoint(new Vector2Int(PositionX, PositionY), 0.4f));
                    }
                }
            }
            
        }
    }
    
    public void ReverseSelected()
    {
        IsSelected = !IsSelected;
        if (IsSelected)
            SpriteRenderer.color = SelectedTileColor;
        else
            SpriteRenderer.color = UnselectedTileColor;
    }

    public void SetPosition(int posX,int posY)
    {
        PositionX = posX;
        PositionY = posY;
    }

    public Shape GetShapeFromTile()
    {
        if (transform.childCount > 0)
            return transform.GetChild(0).GetComponent<Shape>();
        else
            return null;
    }
}
