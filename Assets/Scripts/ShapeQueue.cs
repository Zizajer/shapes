﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeQueue : MonoBehaviour
{
    public Sprite[] SpriteShapes;
    public Color[] colors;
    public GameObject Shape;
    [SerializeField]
    private Transform[] queuePoints;
    private Queue<GameObject> queueOfShapes;
    public Color[] IdentifiedColors;
    private int identifiedColorsCurrentIndex;
    public Sprite[] IdentifiedShapes;
    private int identifiedShapesCurrentIndex;

    // Start is called before the first frame update
    void Start()
    {
        identifiedColorsCurrentIndex = 0;
        identifiedShapesCurrentIndex = 0;
        queueOfShapes = new Queue<GameObject>();
        
    }
    
    public void InitializeQueueInRandom()
    {
        foreach (Transform point in queuePoints)
        {
            queueOfShapes.Enqueue(Instantiate(GetRandomShape(), point.position, new Quaternion(0, 0, 0, 0)));
        }
    }

    public void AddToQueueSavedShape(int positionIndex, GameObject shape)
    {
            queueOfShapes.Enqueue(Instantiate(shape, queuePoints[positionIndex].position, new Quaternion(0, 0, 0, 0)));
    }

    public List<ShapeData> GetQueueShapeDatas()
    {
        List<ShapeData> shapeDatas = new List<ShapeData>();
        int index = 0;
        foreach (GameObject gameObject in queueOfShapes.ToArray())
        {
            shapeDatas.Add(new ShapeData(new Vector2Int(index,0),DefineColorNumber(gameObject.GetComponent<Shape>().GetColor()), DefineShapeNumber(gameObject.GetComponent<Shape>().GetSprite()),false));
            index++;
        }

        return shapeDatas;
    }

    public GameObject GenerateShape(int colorIndex, int shapeIndex)
    {
        Shape.GetComponent<SpriteRenderer>().sprite = SpriteShapes[shapeIndex];
        Shape.GetComponent<SpriteRenderer>().color = colors[colorIndex];
        return Shape;
    }

    public int DefineColorNumber(Color color)
    {
        return Array.IndexOf(colors, color);
    }

    public int DefineShapeNumber(Sprite shape)
    {
        return Array.IndexOf(SpriteShapes, shape);
    }

    public GameObject GetNextShape()
    {
        GameObject returnedObject = queueOfShapes.Dequeue();
        Destroy(returnedObject);
        ChangeDisplayOrder();
        return returnedObject;
    }

    private void ChangeDisplayOrder()
    {
        int i = 0;
        foreach (GameObject gameObject in queueOfShapes.ToArray())
        {
            gameObject.transform.position = queuePoints[i].position;
            i++;
        }
        queueOfShapes.Enqueue(Instantiate(GetRandomShape(), queuePoints[queuePoints.Length-1].position, new Quaternion(0, 0, 0, 0)));
    }

    private GameObject GetRandomShape()
    {
        try
        {
            if (identifiedShapesCurrentIndex < IdentifiedShapes.Length && IdentifiedShapes.Length > 0 && IdentifiedShapes != null)
            {
                Shape.GetComponent<SpriteRenderer>().sprite = IdentifiedShapes[identifiedShapesCurrentIndex];
                identifiedShapesCurrentIndex++;
            }
            else
                Shape.GetComponent<SpriteRenderer>().sprite = GetRandomSpriteShape();

        }
        catch (System.NullReferenceException)
        {
            Shape.GetComponent<SpriteRenderer>().sprite = GetRandomSpriteShape();
        }

        try
        {
            if (identifiedColorsCurrentIndex < IdentifiedColors.Length && IdentifiedColors.Length > 0 && IdentifiedShapes != null)
            {
                Shape.GetComponent<SpriteRenderer>().color = IdentifiedColors[identifiedColorsCurrentIndex];
                identifiedColorsCurrentIndex++;
            }
            else
                Shape.GetComponent<SpriteRenderer>().color = GetRandomColor();
        }
        catch (System.NullReferenceException)
        {
            Shape.GetComponent<SpriteRenderer>().color = GetRandomColor();
        }

        return Shape;
    }


    private Sprite GetRandomSpriteShape()
    {
        int randomNumber = UnityEngine.Random.Range(0, SpriteShapes.Length);
        return SpriteShapes[randomNumber];
    }


    private Color GetRandomColor()
    {
        int randomNumber = UnityEngine.Random.Range(0, colors.Length);
        return colors[randomNumber];
        
    }
}
