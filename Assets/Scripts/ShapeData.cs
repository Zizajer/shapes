﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ShapeData 
{
    public int[] Position;
    public int ColorNumber;
    public int ShapeNumber;
    public bool IsOnTile;

    public ShapeData(Vector2Int position,int colorNumber, int shapeNumber, bool isOnTile)
    {
        Position = new int[2];
        Position[0] = position.x;
        Position[1] = position.y;

        ColorNumber = colorNumber;
        ShapeNumber = shapeNumber;
        IsOnTile = isOnTile;
    }

}
