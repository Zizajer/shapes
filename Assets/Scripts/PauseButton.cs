﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseButton : MonoBehaviour
{
    public GameObject PauseGamePanel;
    public UIManager UIManager;

    private void OnMouseDown()
    {
        UIManager.ActivePauseGamePanel();
    }
}
