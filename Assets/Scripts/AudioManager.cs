﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioClip CantMoveThereClip;
    public AudioClip LineDetectedClip;
    public AudioClip ShapeSelectedClip;
    public AudioClip FreeTileSelectedClip;
    private AudioSource audio;
    public bool isSoundEffectsOn;

    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
        isSoundEffectsOn = (PlayerPrefs.GetInt("isSoundEffectsOn") == 1) ? true:false ;
    }

    public void PlayCantMoveThereClip()
    {
        Camera.main.GetComponent<Animation>().Play();
        if(isSoundEffectsOn)
            audio.PlayOneShot(CantMoveThereClip);
    }

    public void PlayLineDetectedClip()
    {
        if (isSoundEffectsOn)
            audio.PlayOneShot(LineDetectedClip);
        Vibration.Vibrate(20);
    }

    public void PlayShapeSelectedClip()
    {
        if (isSoundEffectsOn)
            audio.PlayOneShot(ShapeSelectedClip);
    }

    public void PlayFreeTileSelectedClip()
    {
        if (isSoundEffectsOn)
            audio.PlayOneShot(FreeTileSelectedClip);
    }

}
