﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Text scoreText;
    private int currentScore = 0;

    public void AddToScore(int amount)
    {
        currentScore += amount;

        AchievementManager.CheckScoreInOneGameAchievement(currentScore);

        scoreText.text = currentScore.ToString();
    }

    public int GetScore()
    {
        return currentScore;
    }
}
