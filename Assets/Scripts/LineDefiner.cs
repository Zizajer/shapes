﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDefiner
{
    public enum LineCheckingDirections
    {
        Horizontally,
        Vertically,
        AslantLeft,
        AslantRight
    };

    public static List<Tile> CheckForLine(GameObject[,] tiles, Tile checkedTile, LineCheckingDirections direction)
    {
        Tile currentCheckedTile = checkedTile;
        bool isAllChecked = false;
        int incrementor = -1;
        List<Tile> shapesInLine = new List<Tile>();
        shapesInLine.Add(currentCheckedTile);
        Tile nextTile;

        while (!isAllChecked)
        {
            if (incrementor < 0)
            {
                try
                {
                    nextTile = GetNextTile(direction, incrementor, currentCheckedTile, tiles);

                    if (AreShapesCompatible(currentCheckedTile.GetShapeFromTile(), nextTile.GetShapeFromTile()))
                    {
                        currentCheckedTile = nextTile;
                        if (!shapesInLine.Contains(currentCheckedTile))
                            shapesInLine.Add(currentCheckedTile);
                    }
                    else
                    {
                        incrementor = 1;
                        currentCheckedTile = checkedTile;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    incrementor = 1;
                    currentCheckedTile = checkedTile;
                }
            }
            else if (incrementor > 0)
            {
                try
                {
                    nextTile = GetNextTile(direction, incrementor, currentCheckedTile, tiles);

                    if (AreShapesCompatible(currentCheckedTile.GetShapeFromTile(), nextTile.GetShapeFromTile()))
                    {
                        currentCheckedTile = nextTile;
                        if (!shapesInLine.Contains(currentCheckedTile))
                            shapesInLine.Add(currentCheckedTile);
                    }
                    else
                    {
                        incrementor = 0;
                        isAllChecked = true;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    incrementor = 0;
                    isAllChecked = true;
                }
            }
        }

        return shapesInLine;
    }

    private static Tile GetNextTile(LineCheckingDirections direction, int incrementor, Tile currentCheckedTile, GameObject[,] tiles)
    {
        Tile nextTile;

        if (direction == LineCheckingDirections.Vertically)
            nextTile = tiles[currentCheckedTile.PositionX + incrementor, currentCheckedTile.PositionY]
                .GetComponent<Tile>();
        else if (direction == LineCheckingDirections.Horizontally)
            nextTile = tiles[currentCheckedTile.PositionX, currentCheckedTile.PositionY + incrementor]
                .GetComponent<Tile>();
        else if (direction == LineCheckingDirections.AslantLeft)
            nextTile = tiles[currentCheckedTile.PositionX + incrementor, currentCheckedTile.PositionY + incrementor]
                .GetComponent<Tile>();
        else
            nextTile = tiles[currentCheckedTile.PositionX - incrementor, currentCheckedTile.PositionY + incrementor]
                .GetComponent<Tile>();

        return nextTile;
    }

    private static bool AreShapesCompatible(Shape firstShape, Shape secondShape)
    {
        if (firstShape != null && secondShape != null)
        {
            if (firstShape.SpriteRenderer.sprite == secondShape.SpriteRenderer.sprite)
                return true;
            else if (firstShape.SpriteRenderer.color == secondShape.SpriteRenderer.color)
                return true;
            else
                return false;
        }
        else
            return false;
    }
}
