﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundSound : MonoBehaviour
{
    private static BackGroundSound instance = null;
    public static BackGroundSound Instance
    {
        get { return instance; }
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    private void Update()
    {
       bool isMusicOn = (PlayerPrefs.GetInt("isMusicOn") == 1) ? true : false;

        if (isMusicOn)
            GetComponent<AudioSource>().volume = 0.5f;
        else
            GetComponent<AudioSource>().volume = 0;
    }


}
