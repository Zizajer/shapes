﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathGenerator
{
    // Start is called before the first frame update

    public GameManager gameManager;

    public PathGenerator(GameManager gm)
    {
        gameManager = gm;
    }

    public List<Tile> GeneratePath()
    {
        List<Tile> openList = new List<Tile>();
        HashSet<Tile> closedList = new HashSet<Tile>();
        Tile currentTile = gameManager.FirstSelectedTile.GetComponent<Tile>();
        currentTile.DistanceToTarget = GetDistance(gameManager.FirstSelectedTile.transform, gameManager.SecondSelectedTile.transform);
        openList.Add(currentTile);
        while (openList.Count > 0)
        {
            currentTile = openList[0];
            for (int i = 1; i < openList.Count; i++)
            {
                if (openList[i].DistanceToTarget < currentTile.DistanceToTarget)
                {
                    currentTile = openList[i];
                }
            }

            openList.Remove(currentTile);
            closedList.Add(currentTile);
            if (currentTile == gameManager.SecondSelectedTile.GetComponent<Tile>())
            {
                return RetracePath(gameManager.FirstSelectedTile.GetComponent<Tile>(), gameManager.SecondSelectedTile.GetComponent<Tile>());
            }

            foreach (Tile neighbour in GetNeighbours(currentTile))
            {
                if (neighbour.GetComponent<Tile>().IsShapeAcquired || closedList.Contains(neighbour))
                {
                    continue;
                }

                float newDistance = GetDistance(neighbour.transform, gameManager.SecondSelectedTile.transform);
                if (currentTile.DistanceToTarget > newDistance || !openList.Contains(neighbour))
                {
                    neighbour.DistanceToTarget = newDistance;
                    neighbour.Parent = currentTile;

                    if (!openList.Contains(neighbour))
                        openList.Add(neighbour);
                }
            }
        }
        return null;
    }

    private List<Tile> RetracePath(Tile startTile, Tile endTile)
    {
        List<Tile> path = new List<Tile>();
        Tile currentTile = endTile;

        while (currentTile != startTile)
        {
            path.Add(currentTile);
            currentTile = currentTile.Parent;
        }
        path.Reverse();
        return path;
    }

    private List<Tile> GetNeighbours(Tile currentTile)
    {
        List<Tile> neighbours = new List<Tile>();
        if (currentTile.PositionX > 0)
            neighbours.Add(gameManager.tiles[currentTile.PositionX - 1, currentTile.PositionY].GetComponent<Tile>());
        if (currentTile.PositionX < gameManager.StartSizeX - 1)
            neighbours.Add(gameManager.tiles[currentTile.PositionX + 1, currentTile.PositionY].GetComponent<Tile>());
        if (currentTile.PositionY > 0)
            neighbours.Add(gameManager.tiles[currentTile.PositionX, currentTile.PositionY - 1].GetComponent<Tile>());
        if (currentTile.PositionY < gameManager.StartSizeY - 1)
            neighbours.Add(gameManager.tiles[currentTile.PositionX, currentTile.PositionY + 1].GetComponent<Tile>());
        return neighbours;
    }

    private float GetDistance(Transform first, Transform second)
    {
        return Vector2.Distance(first.position, second.position);
    }
}
