﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public int StartNumberOfShapesOnGrid;
    public ShapeQueue ShapeQueue;
    public GameObject Tile;
    public int StartSizeX;
    public int StartSizeY;
    [HideInInspector]
    public GameObject[,] tiles;
    [HideInInspector]
    public enum GameStates {NoneSelected ,FirstSelected, SecondSelected, ShapeMoving, AnimationPlaying, Ended};
    public GameStates gameState;
    [HideInInspector]
    public GameObject SelectedShape;
    [HideInInspector]
    public GameObject FirstSelectedTile;
    [HideInInspector]
    public GameObject SecondSelectedTile;
    public Score Score;
    public UIManager uIManager;
    public Vector2Int[] IdentifiedPositions;
    private int IdentifiedPositionsCurrentIndex;
    public TutorialManager TutorialManager;
    private int consecutiveMovesCount;

    // Start is called before the first frame update
    void Start()
    {
        tiles = new GameObject[StartSizeX, StartSizeY];
        float offset = 0.576f;
        for(int x = 0;x < StartSizeX; x++)
        {
            for (int y = 0; y < StartSizeY; y++)
            {
                tiles[x, y] = Instantiate(Tile, new Vector3(transform.position.x + (x * offset), transform.position.y + (y * offset), 0), new Quaternion(0, 0, 0, 0), transform);
                tiles[x, y].GetComponent<Tile>().SetPosition(x, y);
            }
            
        }

        IdentifiedPositionsCurrentIndex = 0;

        SavedGameModel savedGameModel = SaveSystem.LoadPreviousGameFromFile();
        if (savedGameModel != null && TutorialManager == null)
        {
            foreach (ShapeData shapeData in savedGameModel.ShapeDatas)
            {
                if (shapeData.IsOnTile)
                {
                   GameObject freeTile = tiles[shapeData.Position[0], shapeData.Position[1]];
                   Instantiate(ShapeQueue.GenerateShape(shapeData.ColorNumber,shapeData.ShapeNumber), freeTile.transform);
                   freeTile.GetComponent<Tile>().IsShapeAcquired = true;
                }
                else
                {
                    ShapeQueue.AddToQueueSavedShape(shapeData.Position[0], ShapeQueue.GenerateShape(shapeData.ColorNumber, shapeData.ShapeNumber));
                }
            }

            consecutiveMovesCount = 0;

            Score.AddToScore(savedGameModel.CurrentScore);
        }
        else
        {
            ShapeQueue.InitializeQueueInRandom();

            for (int i = 0; i < StartNumberOfShapesOnGrid; i++)
            {
                AddShapeToGrid();
            }

            if (TutorialManager != null)
            {
                StartCoroutine(GoToNextTutorialPoint(new Vector2Int(0, 0), 0.5f));
            }
        }
    }
    
    public void AddShapesAndCheckForLines()
    {
        AddShapeAndCheckForLines(0,StartNumberOfShapesOnGrid);
        gameState = GameManager.GameStates.NoneSelected;
    }

    private void AddShapeAndCheckForLines(int currentIndex, int maxIndex) {
        if (currentIndex < maxIndex) {
            Tile recentlyAdded = AddShapeToGrid();
            if (CheckForLines(recentlyAdded))
            {
                AchievementManager.UnlockNewAddedBlockAchievement();
                StartCoroutine(LaunchMethodAfterTime(currentIndex + 1, maxIndex));
            }
            else
                AddShapeAndCheckForLines(currentIndex + 1, maxIndex);
        }                
            
    }

    public Tile AddShapeToGrid()
    {
        bool isAdded = false;
        Tile recentlyAddedShapeTile = null;
        int randomPositionX = 0;
        int randomPositionY = 0;
        int emptyTilesCount = GetEmptyTilesCount();

        Debug.Log(emptyTilesCount);

        if (emptyTilesCount == 0)
        {
            uIManager.ActiveEndGamePanel(Score.GetScore());
            return recentlyAddedShapeTile;
        }
        if (IdentifiedPositions != null)
        {
            if (IdentifiedPositionsCurrentIndex < IdentifiedPositions.Length && IdentifiedPositions.Length > 0)
            {
                GameObject nextShaoe = ShapeQueue.GetNextShape();
                nextShaoe.transform.position = Vector3.zero;
                nextShaoe.GetComponent<Shape>().enabled = true;
                nextShaoe.GetComponent<Animator>().enabled = true;
                Vector2Int identifiedPosition = IdentifiedPositions[IdentifiedPositionsCurrentIndex];
                IdentifiedPositionsCurrentIndex++;
                GameObject freeTile = tiles[identifiedPosition.x, identifiedPosition.y];
                Instantiate(nextShaoe, freeTile.transform);
                recentlyAddedShapeTile = freeTile.GetComponent<Tile>();
                recentlyAddedShapeTile.IsShapeAcquired = true;
                return recentlyAddedShapeTile;
            }
        }
        

        if (emptyTilesCount > 3)
        {
            while (!isAdded)
            {
                randomPositionX = Random.Range(0, StartSizeX);
                randomPositionY = Random.Range(0, StartSizeY);

                if (!tiles[randomPositionX, randomPositionY].GetComponent<Tile>().IsShapeAcquired)
                {
                    GameObject gameObject = ShapeQueue.GetNextShape();
                    gameObject.transform.position = Vector3.zero;
                    gameObject.GetComponent<Shape>().enabled = true;
                    gameObject.GetComponent<Animator>().enabled = true;
                    Instantiate(gameObject, tiles[randomPositionX, randomPositionY].transform);
                    recentlyAddedShapeTile = tiles[randomPositionX, randomPositionY].GetComponent<Tile>();
                    recentlyAddedShapeTile.IsShapeAcquired = true;
                    isAdded = true;
                }
            }
        }
        else
        {
            GameObject gameObject = ShapeQueue.GetNextShape();
            gameObject.transform.position = Vector3.zero;
            gameObject.GetComponent<Shape>().enabled = true;
            gameObject.GetComponent<Animator>().enabled = true;
            GameObject freeTile = FindNearestLeftFreeTile();
            Instantiate(gameObject, freeTile.transform);
            recentlyAddedShapeTile = freeTile.GetComponent<Tile>();
            recentlyAddedShapeTile.IsShapeAcquired = true;
            isAdded = true;
        }

        return recentlyAddedShapeTile;
    }

    public bool CheckForLines(Tile checkedTile)
    {
        bool isLineFounded = false;
        int linesInOneMove = 0;

        List<Tile> shapesInLineVertically = LineDefiner.CheckForLine(tiles, checkedTile, LineDefiner.LineCheckingDirections.Vertically);
        List<Tile> shapesInLineHorizotally = LineDefiner.CheckForLine(tiles, checkedTile, LineDefiner.LineCheckingDirections.Horizontally);
        List<Tile> shapesInAslantLeft = LineDefiner.CheckForLine(tiles, checkedTile, LineDefiner.LineCheckingDirections.AslantLeft);
        List<Tile> shapesInAslantRight = LineDefiner.CheckForLine(tiles, checkedTile, LineDefiner.LineCheckingDirections.AslantRight);

        HashSet<Tile> tilesToDestroy = new HashSet<Tile>();


        if (CheckIsLineFounded(shapesInLineVertically, tilesToDestroy, linesInOneMove))
            isLineFounded = true;

        if (CheckIsLineFounded(shapesInLineHorizotally, tilesToDestroy, linesInOneMove))
            isLineFounded = true;

        if (CheckIsLineFounded(shapesInAslantLeft, tilesToDestroy, linesInOneMove))
            isLineFounded = true;

        if (CheckIsLineFounded(shapesInAslantRight, tilesToDestroy, linesInOneMove))
            isLineFounded = true;

        foreach (Tile tile in tilesToDestroy)
        {
            tile.IsShapeAcquired = false;
        }

        foreach (Tile tile in tilesToDestroy.OrderBy(tile => tile.transform.position.x).OrderBy(tile => tile.transform.position.y))
        {
            tile.GetShapeFromTile().PlayCollapseAnimation();
        }

        if (isLineFounded)
            consecutiveMovesCount++;
        else
            consecutiveMovesCount = 0;

        if (TutorialManager == null)
        {
            AchievementManager.CheckLinesInOneMoveAchievement(linesInOneMove);
            AchievementManager.CheckConsectiveMovesAchievement(consecutiveMovesCount);
        }

        return isLineFounded;
    }

    private bool CheckIsLineFounded(List<Tile> shapesInLine, HashSet<Tile> tilesToDestroy, int linesInOneMove)
    {
        bool isLineFounded = false;

        if (shapesInLine.Count >= 5)
        {
            GetComponent<AudioManager>().PlayLineDetectedClip();
            Score.AddToScore(ScoreDefiner.GetScoreForLine(shapesInLine));
            foreach (Tile tile in shapesInLine)
            {
                tilesToDestroy.Add(tile);
            }
            if (TutorialManager == null)
            {
                linesInOneMove++;
                AchievementManager.IncrementLineAchievement();
                AchievementManager.CheckShapesNumberInOneLineAchievement(shapesInLine.Count);
            }
        }

        return isLineFounded;
    }

    private int GetEmptyTilesCount() {
        int count = 0;
        foreach(GameObject tile in tiles)
        {
            if (!tile.GetComponent<Tile>().IsShapeAcquired)
            {
                count++;
            }
        }
        return count-1;
    }

    private GameObject FindNearestLeftFreeTile()
    {
        bool isFound = false;
        GameObject tile = null;
        while (!isFound)
        {
            for (int x = 0; x < StartSizeX; x++)
            {
                for (int y = 0; y < StartSizeY; y++)
                {
                    if (!tiles[x, y].GetComponent<Tile>().IsShapeAcquired)
                    {
                        if (isFound)
                            break;

                        tile = tiles[x, y];
                        isFound = true;
                    }
                }

            }
        }
        return tile;
    }

    private IEnumerator LaunchMethodAfterTime(int currentIndex, int maxindex)
    {
        yield return new WaitForSecondsRealtime(1);

        AddShapeAndCheckForLines(currentIndex, maxindex);
    }

    public IEnumerator GoToNextTutorialPoint(Vector2Int lastPosition,float delayedTime)
    {
        yield return new WaitForSecondsRealtime(delayedTime);

        TutorialManager.GoToNextPoint(tiles, lastPosition);
    }

    public SavedGameModel SaveCurrentGame()
    {
        List<ShapeData> shapesInCurrentGame = new List<ShapeData>();

        foreach(GameObject tileGameObject in tiles)
        {
            Tile tile = tileGameObject.GetComponent<Tile>();
            if (tile.IsShapeAcquired)
                shapesInCurrentGame.Add(new ShapeData(new Vector2Int(tile.PositionX, tile.PositionY),ShapeQueue.DefineColorNumber(tile.GetShapeFromTile().GetColor()),ShapeQueue.DefineShapeNumber(tile.GetShapeFromTile().GetSprite()),true));
        }

        foreach(ShapeData queueShapeData in ShapeQueue.GetQueueShapeDatas())
        {
            shapesInCurrentGame.Add(queueShapeData);
        }

        SavedGameModel savedGame = new SavedGameModel(shapesInCurrentGame,Score.GetScore());

        return savedGame;
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause && TutorialManager==null)
        {
            SaveSystem.SaveCurrentGameToFile(SaveCurrentGame());
        }
    }
}
