﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey("HighScore"))
        {
            PlayerPrefs.SetInt("HighScore", 0);
        }

        if (!PlayerPrefs.HasKey("isMusicOn"))
        {
            PlayerPrefs.SetInt("isMusicOn", 1);
        }

        if (!PlayerPrefs.HasKey("isSoundEffectsOn"))
        {
            PlayerPrefs.SetInt("isSoundEffectsOn", 1);
        }

        if (!PlayerPrefs.HasKey("IsTutorialCompleted"))
        {
            PlayerPrefs.SetInt("IsTutorialCompleted", 0);
        }

        if (!PlayerPrefs.HasKey("IsPlayerLogOn"))
        {
            PlayerPrefs.SetInt("IsPlayerLogOn", 0);
        }
    }
}
