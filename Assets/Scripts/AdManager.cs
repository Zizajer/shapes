﻿using GoogleMobileAds.Api;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdManager : MonoBehaviour
{

    private InterstitialAd interstitial;
    
    
    // Start is called before the first frame update
    void Start()
    {
        #if UNITY_ANDROID
                string appId = "ca-app-pub-1403399866294990~5313581838";
        #elif UNITY_IPHONE
                    string adUnitId = "to-do";
        #else
                    string adUnitId = "unexpected_platform";
        #endif

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);

        RequestInterstitial();
    }


    private void RequestInterstitial()
    {
        #if UNITY_ANDROID
                string adUnitId = "ca-app-pub-1403399866294990/2193767931";
#elif UNITY_IPHONE
                    string adUnitId = "to-do";
#else
                    string adUnitId = "unexpected_platform";
#endif
        //if interstitial ad exists
        if (interstitial != null)
            interstitial.Destroy();

        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(adUnitId);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);
    }

    public void ShowInterstitialAd()
    {
        if (this.interstitial.IsLoaded())
        {
            this.interstitial.Show();
        }
    }
}
