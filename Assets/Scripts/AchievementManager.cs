﻿using GooglePlayGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementManager : MonoBehaviour
{
    public static void IncrementLineAchievement()
    {
        PlayGamesPlatform.Instance.IncrementAchievement(
        GPGSIds.achievement_beginner, 1, (bool success) => {
            // handle success or failure
        });

        PlayGamesPlatform.Instance.IncrementAchievement(
        GPGSIds.achievement_rookie, 1, (bool success) => {
            // handle success or failure
        });

        PlayGamesPlatform.Instance.IncrementAchievement(
        GPGSIds.achievement_veteran, 1, (bool success) => {
            // handle success or failure
        });

        PlayGamesPlatform.Instance.IncrementAchievement(
        GPGSIds.achievement_champion, 1, (bool success) => {
            // handle success or failure
        });

        PlayGamesPlatform.Instance.IncrementAchievement(
        GPGSIds.achievement_master, 1, (bool success) => {
            // handle success or failure
        });

        PlayGamesPlatform.Instance.IncrementAchievement(
        GPGSIds.achievement_god, 1, (bool success) => {
            // handle success or failure
        });
    }

    public static void CheckShapesNumberInOneLineAchievement(int shapesNumberInLine)
    {
        switch (shapesNumberInLine)
        {
            case 6:
                Social.ReportProgress(GPGSIds.achievement_sixer, 100.0f, (bool success) => {
                    // handle success or failure
                });
                break;
            case 7:
                Social.ReportProgress(GPGSIds.achievement_711, 100.0f, (bool success) => {
                    // handle success or failure
                });
                break;
            case 8:
                Social.ReportProgress(GPGSIds.achievement_8ball, 100.0f, (bool success) => {
                    // handle success or failure
                });
                break;
            case 9:
                Social.ReportProgress(GPGSIds.achievement_cloud_nine, 100.0f, (bool success) => {
                    // handle success or failure
                });
                break;
        }
    }

    public static void CheckLinesInOneMoveAchievement(int numberOfLines)
    {
        switch (numberOfLines)
        {
            case 2:
                Social.ReportProgress(GPGSIds.achievement_double_trouble, 100.0f, (bool success) => {
                    // handle success or failure
                });
                break;
            case 3:
                Social.ReportProgress(GPGSIds.achievement_three_is_a_crowd, 100.0f, (bool success) => {
                    // handle success or failure
                });
                break;
        }
    }

    public static void CheckConsectiveMovesAchievement(int consectiveMoves)
    {
        switch (consectiveMoves)
        {
            case 3:
                Social.ReportProgress(GPGSIds.achievement_one_two_three, 100.0f, (bool success) => {
                    // handle success or failure
                });
                break;
            case 5:
                Social.ReportProgress(GPGSIds.achievement_just_five, 100.0f, (bool success) => {
                    // handle success or failure
                });
                break;
        }
    }

    public static void CheckScoreInOneGameAchievement(int scoreInThisGame)
    {
        if(scoreInThisGame > 1000)
        {
            Social.ReportProgress(GPGSIds.achievement_not_great_not_terrible, 100.0f, (bool success) => {
                // handle success or failure
            });
        }

        if (scoreInThisGame > 2500)
        {
            Social.ReportProgress(GPGSIds.achievement_pretty_fly, 100.0f, (bool success) => {
                // handle success or failure
            });
        }

        if (scoreInThisGame > 5000)
        {
            Social.ReportProgress(GPGSIds.achievement_looking_good, 100.0f, (bool success) => {
                // handle success or failure
            });
        }

        if (scoreInThisGame > 10000)
        {
            Social.ReportProgress(GPGSIds.achievement_its_over_9000, 100.0f, (bool success) => {
                // handle success or failure
            });
        }

        if (scoreInThisGame > 50000)
        {
            Social.ReportProgress(GPGSIds.achievement_proud_of_you, 100.0f, (bool success) => {
                // handle success or failure
            });
        }

        if (scoreInThisGame > 100000)
        {
            Social.ReportProgress(GPGSIds.achievement_best_of_the_best, 100.0f, (bool success) => {
                // handle success or failure
            });
        }
    }

    public static void UnlockNewAddedBlockAchievement()
    {
        Social.ReportProgress(GPGSIds.achievement_pure_luck, 100.0f, (bool success) => {
            // handle success or failure
        });
    }

    public static void UnlockLineInSameShapekAchievement()
    {
        Social.ReportProgress(GPGSIds.achievement_shape_of_my_heart, 100.0f, (bool success) => {
            // handle success or failure
        });
    }

    public static void UnlockLineInSameColorkAchievement()
    {
        Social.ReportProgress(GPGSIds.achievement_colors_of_the_wind, 100.0f, (bool success) => {
            // handle success or failure
        });
    }

    public static void UnlockLineInSameColorAndShapekAchievement()
    {
        Social.ReportProgress(GPGSIds.achievement_one_to_rule_them_all, 100.0f, (bool success) => {
            // handle success or failure
        });
    }
}
